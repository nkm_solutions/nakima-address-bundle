# Como crear un proyecto

Primero de todo, usaremos este repositorio para obtener el codigo base del proyecto.

1. Clonas este repositorio `git clone git@bitbucket.org:nkm_solutions/basephpproject.git my-proyect`
2. `cd my-proyect`
3. `rm -rf .git`
4. `composer update`
5. Opcionalmente `composer autoload dump`
6. Abres el archivo composer.json y se deben cambiar:
    * 'Base' por el namespace base de tu proyecto
    * "name": "vivi/nakima-_______-bundle"

Una vez tenemos el codigo base, procedemos a añadirlo en el nuevo repositorio.

1. `git init`
2. `git remote add origin git@bitbucket.org:nkm_solutions/____________.git`
3. `git add .`
4. `git commit -m "Initial commit with BasePHPProject"`
5. `git push -u origin master`

Luego, si queremos añadir éste repositorio en **PACKAGIST**, haremos los siguientes pasos:

1. Navegamos a: https://packagist.org/packages/submit
2. Añadimos la URL **HTTP** del repositorio **sin el USERNAME**
3. Auto-update: Añadimos el siguiente webhook
    * Title: Packagist
    * URL: https://packagist.org/api/bitbucket?username=vivi&apiToken=WqjW4CatDvFTevRZsgFh

Por último, añadimos el repositoria a symfony via composer.

1. composer require vivi/nakima-_______-bundle dev-master

Y con esto ya estamos listos :)

---

# Como crear un nuevo Bundle de Symfony 3 from Scrach

Su poninedo que el nuevo Bundle se llama: __*NakimaAddressBundle*__

1. Renombrar el archivo "src/Main.php" a "src/NakimaAddressBundle.php"

        #!php
        <?php
        namespace Nakima\AddressBundle;

        use Symfony\Component\HttpKernel\Bundle\Bundle;

        class NakimaAddressBundle extends Bundle {}

2. Creamos la carpeta bundle, dentro de la cual pondremos los fixeros que se instalaran en un proyecto concretro.

3. Create "bundle/AddressBundle.php"

        #!php
        <?php
        namespace AddressBundle;

        use Symfony\Component\HttpKernel\Bundle\Bundle;

        class AddressBundle extends Bundle {}

4. create config files

    * src
        * src/Resources/config/admin.yml
        * src/Resources/config/config.yml
        * src/Resources/config/routing.yml

    * bundle
        * bundle/Resources/config/admin.yml

                #!yaml
                imports:
                    - { resource: "@NakimaAddressBundle/Resources/config/admin.yml" }

        * bundle/Resources/config/config.yml

                #!yaml
                imports:
                    - { resource: "@NakimaAddressBundle/Resources/config/config.yml" }

        * bundle/Resources/config/routing.yml

                #!yaml
                nakima_address:
                    resource: "@NakimaAddressBundle/Resources/config/routing.yml"
                    prefix:   /

5. Modificar "AppKernel.php"

        #!yaml
        ...

        // NAKIMA
        ...
        new Nakima\AddressBundle\NakimaAddressBundle(),

        ...

6. Añadir en el actual proyecto el Bundle creado. Ejecutar:

        #!bash
        php bin/console nakima:bundle:install address

7. En el proyecto donde hacemos el requide, modificar el archivo: "app/config/config.yml". Y añadir:

        #!yaml
        imports:
            ...

            # Import bundles config
            ...
            - { resource: "@AddressBundle/Resources/config/config.yml" }

            # Import admin settings
            ...
            - { resource: "@AddressBundle/Resources/config/admin.yml" }

            ...

8. Modificar "AppKernel.php"

        #!yaml
        ...

        // NAKIMA
        ...

        ...
        new AddressBundle\AddressBundle(),

        ...

## Nueva Entity

1. "src/Entity/Adress.php"

        #!php
        <?php
        namespace Nakima\AddressBundle\Entity;

        use Doctrine\ORM\Mapping\Entity;
        ...

        use Nakima\CoreBundle\Entity\BaseEntity;

        /**
         * @MappedSuperclass
         */
        class Address extends BaseEntity {

            ...

            /**************************************************************************
             * Custom Functions                                                       *
             **************************************************************************/

            public function __toString() {
                return $this->_______;
            }

            /**************************************************************************
             * Getters & Setters                                                      *
             **************************************************************************/

             ...
        }

2. "bundle/Entity/Adress.php"

        #!php
        <?php
        namespace AddressBundle\Entity;

        use Doctrine\ORM\Mapping\Entity;
        use Doctrine\ORM\Mapping\Table;

        use Nakima\AddressBundle\Entity\Address AS BaseAddress;

        /**
         * @Entity
         * @Table(name="_address")
         */
        class Address extends BaseAddress {}

3. "src/Resources/config/admin.yml"

        #!yaml
        nakima_admin:
            blocks:
                address_entity:
                    class: Nakima\AdminBundle\Block\EntityBlock
                    meta:
                        type: entity
                        label: Addresses
                        entity: AddressBundle\Entity\Address
                        icon: 'circle-o'
                        icon_color: 'x'
                        # optionals
                        admin: Nakima\AdminBundle\Admin\BaseAdmin
                        controller: NakimaAdminBundle:BaseAdmin

4. Append on "bundle/Resources/config/admin.yml"

        #!yaml
        ...

        nakima_admin:
        blocks:
            address_entity:
                meta:
                    icon_color: 'green'

5. Añadir a mano los cambios realizados dentro el proyecto principal.

6. Check usant schema update

        #!bash
        php bin/console doctrine:schema:update --force
