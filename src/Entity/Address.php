<?php
declare(strict_types=1);
namespace Nakima\AddressBundle\Entity;

/**
 * @author xgc1986 < xgc1986@gmail.com >
 */

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\MappedSuperclass;
use Nakima\CoreBundle\Entity\BaseEntity;
use Nakima\CoreBundle\Utils\Doctrine;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @MappedSuperclass
 */
class Address extends BaseEntity
{

    /**
     * @Column(type="text", nullable=false)
     */
    protected $name;

    /**
     * @Column(type="string", length=64, nullable=false)
     */
    protected $contactName;

    /**
     * @Column(type="string", length=24, nullable=false)
     */
    protected $contactNumber;

    /**
     * @Column(type="string", length=254, nullable=true)
     * @Assert\Email(
     *     checkMX=true,
     *     checkHost=true
     * )
     */
    protected $email;

    /**
     * @Column(type="string", length=256, nullable=false)
     */
    protected $line;

    /**
     * @Column(type="string", length=64, nullable=false)
     */
    protected $city;

    /**
     * @Column(type="string", length=64, nullable=false)
     */
    protected $province;

    /**
     * @Column(type="string", length=16, nullable=false)
     */
    protected $zip;

    /**
     * @ManyToOne(
     *     targetEntity="GeoBundle\Entity\Country"
     * )
     * @JoinColumn(
     *     name="country_id",
     *     referencedColumnName="id",
     *     nullable=false
     * )
     */
    protected $country;

    /**
     * @Column(type="string", length=140, nullable=true)
     */
    protected $details;

    /******************************************************************************************************************
     *                                                                                                                *
     *   Custom Functions                                                                                             *
     *                                                                                                                *
     ******************************************************************************************************************/

    public function __construct()
    {
        parent::__construct();
        $this->setDetails("");
        $this->setCountry(\GeoBundle\Entity\Country::load('Spain'));
    }

    public function __toString()
    {
        $ret = "$this->line, $this->city ($this->zip), $this->country, telf: $this->contactNumber";
        if ($this->details) {
            $ret .= ", (details: $this->details)";
        }
        return $ret;
    }

    public function __toArray(array $options = []): array
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'contactName' => $this->getContactName(),
            'contactNumber' => $this->getContactNumber(),
            'email' => $this->getEmail(),
            'line' => $this->getLine(),
            'city' => $this->getCity(),
            'province' => $this->getProvince(),
            'zip' => $this->getZip(),
            'country' => Doctrine::toArray($this->getCountry()),
            'details' => $this->getDetails(),
        ];
    }

    /******************************************************************************************************************
     *                                                                                                                *
     *   Getters & Setters                                                                                            *
     *                                                                                                                *
     ******************************************************************************************************************/

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    public function getContactName()
    {
        return $this->contactName;
    }

    public function setContactName($contactName)
    {
        $this->contactName = $contactName;

        return $this;
    }

    public function getContactNumber()
    {
        return $this->contactNumber;
    }

    public function setContactNumber($contactNumber)
    {
        $this->contactNumber = $contactNumber;

        return $this;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    public function getLine()
    {
        return $this->line;
    }

    public function setLine($line)
    {
        $this->line = $line;

        return $this;
    }

    public function getCity()
    {
        return $this->city;
    }

    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    public function getProvince()
    {
        return $this->province;
    }

    public function setProvince($province)
    {
        $this->province = $province;

        return $this;
    }

    public function getZip()
    {
        return $this->zip;
    }

    public function setZip($zip)
    {
        $this->zip = $zip;

        return $this;
    }

    public function getCountry()
    {
        return $this->country;
    }

    public function setCountry(\GeoBundle\Entity\Country $country)
    {
        $this->country = $country;

        return $this;
    }

    public function getDetails()
    {
        return $this->details;
    }

    public function setDetails($details)
    {
        $this->details = $details;

        return $this;
    }

}
