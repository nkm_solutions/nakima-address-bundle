<?php
declare(strict_types=1);
namespace Nakima\AddressBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AddressType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('name')
            ->add('contactName')
            ->add('contactNumber')
            ->add('email', null, ['required' => false])
            ->add('line')
            ->add('city')
            ->add('province')
            ->add('zip')
            ->add('country')
            ->add('details', null, ['required' => false])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AddressBundle\Entity\Address',
            'required' => false
        ));
    }
}
