<?php
declare(strict_types=1);
namespace Nakima\AddressBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class NakimaAddressBundle extends Bundle {}
